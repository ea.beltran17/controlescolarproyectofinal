Create database controlescolar
use controlescolar

create table Usuarios
(   
	idusuario int not null identity(1000,1),
	nombre varchar(100) not null,
	Usuario varchar(100) not null,
	edad int not null,
	puesto varchar(100) not null,
	correo varchar(100) not null,
	telefono varchar(100) not null,
	direccion varchar (100)not null,
	contrase�a varchar(100) not null,
	primary key (idusuario)
);
insert into Usuarios(nombre, Usuario,edad,puesto,correo,telefono,direccion,contrase�a) values ('Eduardo Antonio Beltran Pe�a','Eduardo',20,'Administrador','Eduardo@gmail.com','6677959884','Infonavit Humaya Avenida Pedro Infante','12345')

create table Alumnos
(
	matricula int not null identity(1000,1),
	nombre varchar(100) not null,
	apellidos varchar(100) not null,
	telefono varchar(100) not null,
	email varchar(100) not null,
	materiacursa varchar(100) not null,
	grupo varchar(50) not null,
	primary key (matricula)
);
--Alumnos
insert into Alumnos(nombre,apellidos,telefono,email,materiacursa,grupo)values ('Alonso','Campi�a','6675025614','alonso@gmail.com','Economia,Fisica,Quimica,Leyes 1','5-1')
insert into Alumnos(nombre,apellidos,telefono,email,materiacursa,grupo)values ('fernanda','Peres','667xxxxxxx','fernanda@gmail.com','matematicas,graficacion,algoritmos','5-2')
insert into Alumnos(nombre,apellidos,telefono,email,materiacursa,grupo)values ('juan','Campi�a','667xxxxxxx','juan@gmail.com','leyes 3,Programacion,Quimica','5-3')

select * from alumnos
create table Maestros
(
	idmaestro int not null identity(1000,1),
	nombre varchar(100) not null,
	apellidos varchar(100) not null,
	telefono varchar(100) not null,
	email varchar(100) not null,
	materiaimparte varchar(100) not null,
	primary key (idmaestro)
);
--Maestros
insert into Maestros(nombre,apellidos,telefono,email,materiaimparte)values ('Eduardo','velasque','667xxxxxxx','Eduardo@gmail.com','Economia, Programacion')
insert into Maestros(nombre,apellidos,telefono,email,materiaimparte)values ('Ricardo','Pe�a','667xxxxxxx','Ricardo@gmail.com','Leyes 1, Economia')
Select * from Maestros


Create table Materias
(
	idmaterias int not null identity(1000,1),
	nombre varchar (100) not null,
	profesorimparte varchar (100) not null,
	limitealum int not null
	primary key (idmaterias)

); 
--Materias
Insert into Materias(nombre,profesorimparte,limitealum) values('Economia','Ricardo Pe�a',40)
Select * from Materias

create table Horario
(
	idhorario int not null identity(1000,1),
	materias varchar(100) not null,
	profesores varchar(100) not null,
	horainicio varchar(100) not null,
	horafin varchar(100) not null,
	primary key (idhorario)

);
--Horario
insert into Horario(materias,profesores,horainicio,horafin)values('Economia','Eduardo','7:00 Am','1:00 pm')
select * from Horario

create table Calificaciones
(
	idcali int not null identity(1000,1),
	materia varchar(100) not null,
	alumno varchar(100) not null,
	calificacion int not null,
	primary key (idcali)


);
--Calificaciones
insert into Calificaciones(materia,alumno,calificacion) values('Economia','Alonso',10)
Select * from Calificaciones




 